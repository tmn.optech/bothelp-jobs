# Демонстрация тестового nodejs приложения с публикацией в инфарструктурной платформе Kubernetes

## Observability
| Сервис | Описание |
|-|-|
| ELK | Централизованый стек хранения и анализа лог журналов (Daemonset fluent-bit or native librery - watsonhttps://github.com/watson-developer-cloud/node-sdk) |
| Sentry | Трекер трейсбеков |
| DD-trace | APM - трассеровка запросов  |
|Prometheus operator| Система мониторинга pull (сборк метрик) |

## Что учесть на проде K8S.

- PodDisruptionBudgets
- Readiness probe (http probe)
- Affinity pods (распределение pods по нодам, для отказоустойчивости)
- Limits
- Hooks migration
- Node affinity or NoSchedule + tolerations
- Helm secrets (dec configmap)


### Экспозиция метрик приложения и nodejs

Приложение поставляется с endpoints по которому публикуется plain text метрик в поддерживаемом формате Prometheus.
Автоматическое нахождение и добавление targets Prometheus описывается Servicemonitor.

### Метрики:

#### NodeJS Runtime Metrics

| Метрика | Описание |
|-|-|
| runtime.node.cpu.user |CPU usage in user code |
| runtime.node.cpu.system | CPU usage in system code |
| runtime.node.cpu.total |	Total CPU usage  |
| runtime.node.heap.total_heap_size| Total heap size |
| ... | 
 > Полное описание метрик
 > https://docs.datadoghq.com/tracing/runtime_metrics/nodejs/

### Установка сервиса:

1. Chart сервиса необходимо разместить в репозитории charts.

2. Сборка image docker с tag - pipelineID

С каждой ноды кластера должен быть доступен реджистри Docker для работы с образами.

3. Деплой приложения с использование helm и плагином helm secrets (https://github.com/zendesk/helm-secrets)
Примечание: перед helm upgrade and/or install сработает hook - job с миграцией на DB.

### Checks hooks - проверка приложения после миграций.
На будущее использовать проект проверки - возварт exit 0/1 с уведомлением в slack
https://github.com/bitsofinfo/kubernetes-helm-healthcheck-hook
